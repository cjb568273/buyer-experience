const currentDate = new Date();
const firstDayOfYear = new Date(currentDate.getFullYear(), 0, 1);
const days = Math.floor((currentDate - firstDayOfYear) / (24 * 60 * 60 * 1000));

const weekNumber = Math.ceil(days / 7);

console.log(`The current week number is: ${weekNumber}`);

// If it's an odd month (not sprint release month)
if (weekNumber % 2 != 0) {
  process.exit(0);
}
