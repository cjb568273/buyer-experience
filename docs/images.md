#### Using `NuxtImage` configuration
[This plugin](https://image.nuxtjs.org/components/nuxt-img)is an built-in image optimization and allows sizing for images can be done directly within the `<nuxt-img/>` tag. [Screen sizes](https://image.nuxtjs.org/components/nuxt-img) have been predefined and are as listed:
    - xs: 320,
    - sm: 640,
    - md: 768,
    - lg: 1024,
    - xl: 1280,
    - xxl: 1536,
    -'2xl': 1536,

NuxtImage features includes: 
- Modern browser image handling with
- Creating reate multiple image exports based on viewport size
- Resizing image at build time
- Lazy loading/ image compression
- Using next-gen image formats webp
- Fallback png format for older browsers
- Creating a small filesize placeholder image for really slow connections

When to use `<img/>` vs `<nuxt-img/>`
    NuxtImage will throw errors in the build process if it is given js properties that throw errors.
        - This may require you to troubleshoot NuxtImage in a way that you wouldn't have to with a regular image tag i.e.(make sure you're not accessing properties on undefined or undefined values in intermediary states)
    Images with url sources
        - NuxtImage uses IPX for self-hosting this will cause an IPX Error in the build process that will not prevent the site from building but it will fail to load images with url as sources. To use NuxtImage the image will need to be pulled into the repo and the source link updated in the `.yml` file.
---

### Image folders that cannot be deleted

Please DO NOT delete the following images folder because although not referenced directly, they are being programmatically referenced:

- `/nuxt-images/applications/`
- `/nuxt-images/navigation/`
- `/nuxt-images/resources/`
- `/nuxt-images/home/resources/`
- `/nuxt-images/competition/progress-bars/`

