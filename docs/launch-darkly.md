
# Running AB tests with `launch-darkly.vue`

## Background

When we say we want to run an AB test, this means running an experiment between two competing designs. One is called `control` or the existing design; Another is called `experiment` or the change in design. We want to measure the success of both of these variations against real users, and keep the winning variation. We also want to make sure this change didn't happen by chance, and that the difference is significant enough. For example, we want the "Get Free trial" button on the home page's hero to increase by 5%.  

To implement this experiment, we will need to define both at least one **metric** and exactly one **feature flag**. 

- A **feature flag** is used in an AB test to control the what variation to show to users. So far, we have only done 50/50 splits. 
- A **metric** is an event that will fire based on the action that a user can take. This can be a button click, filling out a form, hovering over something, or viewing a certain part of the page. 

### Best practices for getting valid results

1. **Don't** use too many variations at a time.
2. **Don't** change experiment settings in the middle of the experiment
3. **Do** use tools like [this](https://vwo.com/tools/ab-test-duration-calculator/) to track how long you should run an experiment for  


## Running an Experiment
### The `launch-darkly.vue` component 

This component is front-end wrapper to LaunchDarkly's SDK. On the client, it evaluates the value of a *feature flag* and displays the contents of either an `experiment` or `control`.

Go to the page Vue component that you want to run an experiment on, and do the following: 

```javascript
// ~/pages/enterprise.vue
import LaunchDarkly from '~/components/launch-darkly.vue'
```

```javascript
  components: {
    YourOtherComponent,
    LaunchDarkly
  },
```

```vue
<LaunchDarkly featureFlag="your-feature-flag-name-here"> 
  <template #experiment>
  // Your experiment goes here
  </template>
  <template #control>
  // Your control goes here
  </template>
</LaunchDarkly>
```

If you do not propely author the `control` and `experiments` slots, a warning will be thrown out to the `console`. You can force a page to show either the control/experiment by passing `?override-control` or `?ovveride-test` respectively as a URL parameter. 

### Go to the Launchdarkly Interface
1. Login using shared credentials to LaunchDarkly (WIP)
2. Create a feature flag by clicking on Create flag
   1. Make sure feature flag name is descriptive and starts with an issue number (ex: 1040-home-page-banner)
      1. Be sure to add a description of how the control and experiment differ (ex: control CTA is purple, experiment CTA is black)
   2. Set Flag variations to Numbers
      1. 0 for control, 1 for experiment
3. Click on the created feature flag. Change default percentage rule to experiment allocation by clicking on the right-aligned chart icon within the default rule section. Select the "Run experiment on this rule" within the dropdown, and then appropriately allocate the percentages of the control and experient based on the test. 
4. If you aren't using an existing created metric, you will need to create one by clicking on "Experiments" on the left hand-side. Click on the "Metrics" tab, and click on "Create metric". We recommend creating metrics as custom conversions compared to using click events. This is because page redirects will happen before the click event can be sent back to LaunchDarkly.  We will have greater control and ensure that the metric event is captured prior to redirect. If we are sure that a click event will be captured
5. After you have the appropriate metrics, navigate to "Feature flags", click on the relevant feature flag, then the "Experimentation" tab. "Manage Metrics", and add the relevant metric here. 
6. Click on Start experiment. If you get  "Cannot record experiment metrics without first adding an experiment allocation to the associated flag’s targeting rules", you will need to change your percentage rollout to an experiment allocation. To get this view, click on Feature Flag -> Targetting -> Default rule section -> chart icon on the right. Screenshots below. 
7. Verify that an experiment is recording by going to Experiments and see that an experiment is recording. 
![](/docs/images/run-experiment-on-this-rule.png)

![](/docs/images/experiment-allocation.png)


Docs https://docs.launchdarkly.com/home/creating-experiments
## Limitations 

As our Buyer's Experience site is a purely statically generated site, we will need to make a client-side request to resolve the value of a *feature flag*. Without a server to offload that request, there will always be latency between the rest of the page and the contents of the experiment. This introduces a problem where we have to decide whether to flash content of a test on to the page, or display the contents of the entire page once the value of the *feature flag* is resolved. Either of these can negatively affect page speed metrics.

We are currently limited to ten seats, so you may need to reach out to either the managers of Digital Experience or [engineering leads](https://about.gitlab.com/handbook/marketing/digital-experience/#groups-metrics--team-members)
