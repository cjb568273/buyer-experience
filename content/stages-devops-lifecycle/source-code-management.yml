template: feature
title: 'Source Code Management'
description: 'Source Code Management (SCM) in GitLab helps your development team collaborate and maximize productivity, sparking faster delivery and increase visibility.'
components:
  feature-block-hero:
    data:
      title: Source Code Management
      subtitle: GitLab makes Source Code Management easy
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: Start your free trial
        url: /free-trial/
      image:
        image_url: "/nuxt-images/devops-graphics/create.png"
        hide_in_mobile: true
        alt: ""
  side-navigation:
    links:
      - title: Overview
        href: '#overview'
      - title: Benefits
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: Version control in GitLab
          content_summary: >-
            helps your development team collaborate and maximize productivity,
            sparking faster delivery and increased visibility. With its Git-based
            repository, GitLab enables clear code reviews, asset version control,
            feedback loops, and powerful branching patterns to help your developers
            solve problems and ship value.
          content_image: /nuxt-images/features/source-code-management/overview.jpg
          content_heading: Version control for everyone
          content_list:
            - Scale your SDLC for cloud native adoption
            - Git-based repository enables developers to work from a local copy
            - Automatically scan for code quality and security with every commit
            - Built-in Continuous Integration and Continuous Delivery
      feature-benefit:
        data:
          feature_heading: Transform software development
          feature_cards:
            - feature_name: Collaborate
              icon:
                name: collaboration
                variant: marketing
                alt: Collaboration Icon
              feature_description: >-
                Deliver faster... helps your development team collaborate and maximize
                productivity, sparking faster delivery and increased visibility
              feature_list:
                - 'Review, comment, and improve code'
                - Enable re-use and innersourcing.
                - File locking prevents conflicts.
                - Robust WebIDE accelerates development on any platform.
            - feature_name: Accelerate
              icon:
                name: increase
                variant: marketing
                alt: Increase Icon
              feature_description: >-
                Always be launching... asset version control, feedback loops, and
                powerful branching patterns to help your developers solve problems and
                ship value.
              feature_list:
                - Git-based repository enables developers to work from a local copy.
                - 'Branch code, make changes, and merge into the main branch.'
                - Robust WebIDE accelerates development on any platform.
            - feature_name: Compliant and Secure
              icon:
                name: release
                variant: marketing
                alt: Shield Check Icon
              feature_description: >-
                Track and trace... enables teams to manage their work with a single
                source of truth.
              feature_list:
                - >-
                  Review, track, and approve code changes with powerful merge
                  requests.
                - Automatically scan for code quality and security with every commit.
                - >-
                  Simplify auditing and compliance with granular access controls and
                  reporting.
  feature-block-related:
    data:
      - Continuous Integration
      - Epic & Issue Boards
      - Security Dashboards
      - Approval Rules
      - Vulnerability Management
  group-buttons:
    data:
      header:
        text: Explore other ways GitLab can help source code management.
        link:
          text: Explore more solutions
          href: /solutions/
      buttons:
        - text: Delivery automation
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: Continuous integration
          icon_left: continuous-delivery
          href: /features/continuous-integration/
        - text: Continuous software security
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
  solutions-resource-cards:
    data:
      cards:
        - icon:
            name: webcast
            variant: marketing
            alt: Webcast Icon
          event_type: Webcast
          header: Collaboration without Boundaries - Faster Delivery with GitLab
          link_text: Read more
          image: /nuxt-images/features/resources/resources_webcast.png
          href: /partners/technology-partners/aws/
          data_ga_name: Collaboration without Boundaries
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: Case Study Icon
          event_type: Case Study
          header: >-
            GitLab advances open science education at Te Herenga Waka – Victoria
            University of Wellington
          link_text: Read more
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: GitLab advances open science education at Te Herenga Waka
          data_ga_location: body
        - icon:
            name: partners
            variant: marketing
            alt: Partners Icon
          event_type: Partners
          header: Discover the benefits of GitLab on AWS
          link_text: Watch now
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_partners.png
          data_ga_name: Discover the benefits of GitLab on AWS
          data_ga_location: body
