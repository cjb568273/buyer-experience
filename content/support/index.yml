---
title: Support
description: Visit the GitLab support page to find product support links and to contact the support team.
side_menu:
  anchors:
    text: "On this page"
    data:
    - text: Contact Support
      href: "#contact-support"
      data_ga_name: contact support
      data_ga_location: side-navigation
      variant: primary
      nodes:
      - text: Have questions about billing, purchasing, subscriptions or licenses?
        href: "#have-questions-about-billing-purchasing-subscriptions-or-licenses"
      - text: Can’t find what you’re looking for?'
        href: "#cant-find-what-you-are-looking-for"
      - text: Managing your support contacts
        href: "#managing-your-support-contacts"
    - text: GitLab Support Service Levels
      href: "#gitlab-support-service-levels"
      data_ga_name: gitlab support service levels
      data_ga_location: side-navigation
      nodes:
      - text: Trials Support
        href: "#trials-support"
      - text: Standard Support (Legacy)
        href: "#standard-support-legacy"
      - text: Priority Support
        href: "#priority-support"
      - text: How to trigger Emergency Support
        href: "#how-to-trigger-emergency-support"
    - text: Service Level Agreements
      href: "#service-level-agreements"
      data_ga_name: service level agreements
      data_ga_location: side-navigation
    - text: Hours of Operation
      href: "#hours-of-operation"
      data_ga_name: hours of operation
      data_ga_location: side-navigation
      nodes:
      - text: Definitions of GitLab Global Support Hours
        href: "#definitions-of-gitlab-global-support-hours"
      - text: Effect on Support Hours if a preferred region for support is chosen
        href: "#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen"
      - text: Phone and video call support
        href: "#phone-and-video-call-support"
  hyperlinks:
    text: "More resources"
    data:
      - text: "Customer Satisfaction"
        href: "/support/customer-satisfaction/"
        variant: tertiary
        data_ga_name: "Customer Satisfaction"
        data_ga_location: "side-navigation"
        icon: true
      - text: "General Support Rules"
        href: "/support/general-policies/"
        variant: tertiary
        data_ga_name: "General Support Rules"
        data_ga_location: "side-navigation"
        icon: true
      - text: "GitLab.com Specific Support Policies"
        href: "/support/gitlab-com-policies/"
        variant: tertiary
        data_ga_name: "GitLab.com Specific Support Policies"
        data_ga_location: "side-navigation"
        icon: true
      - text: "Support Definitions"
        href: "/support/definitions/"
        variant: tertiary
        data_ga_name: "Support Definitions"
        data_ga_location: "side-navigation"
        icon: true
      - text: "Support Portal"
        href: "/support/portal/"
        variant: tertiary
        data_ga_name: "Support Portal"
        data_ga_location: "side-navigation"
        icon: true
      - text: "US Federal Support"
        href: "/support/us-federal-support/"
        variant: tertiary
        data_ga_name: "US Federal Support"
        data_ga_location: "side-navigation"
        icon: true
support-hero:
  data:
    title: GitLab Support
    content: |
      There are many ways to contact Support, but the first step for most people should be to search our <a href="https://docs.gitlab.com/" data-ga-name="documentation" data-ga-location="header">documentation</a>.
components:
  - name: support-copy-markdown
    data:
      block:
        - header: Contact Support
          id: contact-support
          subtitle:
            id: have-questions-about-billing-purchasing-subscriptions-or-licenses
            text: Have questions about billing, purchasing, subscriptions or licenses?
          text: |
            We’re here to help! No matter what plan you have purchased, GitLab Support will respond within 8 hours on business days (24x5).
            Open a Support Ticket on the [GitLab Support Portal](/support/portal/){data-ga-name="support portal" data-ga-location="header"} and select "[Licensing and Renewals Problems](https://about.gitlab.com/handbook/support/license-and-renewals/){data-ga-name="licensing and renewals problems" data-ga-location="header"}".
        - subtitle:
            id: cant-find-what-you-are-looking-for
            text: Can’t find what you’re looking for?
          text: |
              If you can't find an answer to your question, or you are affected by an outage, then customers who are in a **paid** tier should start by consulting our [statement of support](/support/statement-of-support/){data-ga-name="statement-of-support" data-ga-location="body"} while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done at the discretion of the agent or engineer and is provided as a courtesy.
  - name: support-note
    data:
      icon:
        name: magnifying-glass
        variant: marketing
        alt: Magnifying Glass Icon
      title: Find your support level
      id: find -your-support-level
      options:
        - GitLab Free plan
        - GitLab Premium plan
        - GitLab Ultimate plan
        - GitLab free Ultimate trial
        - GitLab Ultimate for Education
        - GitLab Ultimate for Open Source
        - GitLab Ultimate for Startups
        - Legacy plan – Starter self-managed
        - Legacy plan – Bronze SaaS
        - US Federal – Self-managed
        - Open Partners
        - Select Partners
        - Alliance Partners
      select_menu:
        placeholder: Select your plan or account type
        options:
          - id: 1
            name: GitLab Free plan
            text: |
              <h3>GitLab Free plan</h3>
              <ul>
                <li>GitLab Free plan (SaaS or Self-managed) does not include support at any level. Instead, you can open a thread in the GitLab Community Forum. <a href="https://forum.gitlab.com/" data-ga-name="gitlab community forum" data-ga-location="body">GitLab Community Forum</a> .</li>
                <li>If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the  <a href="https://support.gitlab.com/hc/en-us" data-ga-name="gitlab support portal" data-ga-location="body">GitLab Support Portal</a> and select "<a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" data-ga-name="gitlab support portal" data-ga-location="body">Licensing and Renewals Problems</a>".</li>
              </ul>
          - id: 2
            name: GitLab Premium plan
            text: |
              <h3>GitLab Premium plan</h3>
              <ul>
                <li>GitLab Premium plan comes with <a href="/support/#priority-support">Priority Support</a> for both SaaS and SM</li>
                <li>Tiered reply times based on <a href="/support/definitions/#definitions-of-support-impact">definitions of support impact</a> for SaaS and SM</li>
                <li>Open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447">GitLab Support Portal</a> for SaaS and SM</li>
                <li>For emergency requests, see the note in the <a href="/support/#how-to-trigger-emergency-support">How to Trigger Emergency Support</a> for SM.</li>
              </ul>
          - id: 3
            name: GitLab Ultimate plan
            text: |
              <h3>GitLab Ultimate plan</h3>
              <ul>
                <li>GitLab Ultimate plan comes with <a href="/support/#priority-support">Priority Support</a> for both SaaS and SM</li>
                <li>Tiered reply times based on <a href="/support/definitions/#definitions-of-support-impact">definitions of support impact</a> for SaaS and SM</li>
                <li>Open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447">GitLab Support Portal</a>  for SaaS and SM</li>
                <li>For emergency requests, see the note in the <a href="/support/#how-to-trigger-emergency-support">How to Trigger Emergency Support</a> for SM</li>
              </ul>
          - id: 4
            name: GitLab free Ultimate trial
            text: |
              <h3>Free GitLab Ultimate trial</h3>
              <ul>
                <li>Free GitLab Ultimate Self-managed and SaaS granted through trials do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider <a href="/sales/" data-ga-name="sales" data-ga-location="body">contacting Sales</a> to discuss options.</li>
                <li>If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us" data-ga-name="gitlab support portal" data-ga-location="body">GitLab Support Portal</a> and select "<a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" data-ga-name="gitlab support portal" data-ga-location="body">Licensing and Renewals Problems</a>".</li>
              </ul>
          - id: 5
            name: GitLab Ultimate for Education
            text: |
              <h3>GitLab for Education</h3>
              <ul>
                <li>Please see the <a href="https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs" data-ga-location="body">Support for Community Programs</a> docs sections for a detailed description of where to find support. Please note that it is no longer an option to purchase support separately for GitLab for Education licenses. Instead, qualifying institutions have the option to purchase the <a href="https://about.gitlab.com/solutions/education/campus/" data-ga-location="body">GitLab for Campuses subscription</a>.</li>
                <li>If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us" data-ga-name="gitlab support portal" data-ga-location="body">GitLab Support Portal</a> and select "<a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" data-ga-name="gitlab support portal" data-ga-location="body">Licensing and Renewals Problems</a>".</li>
              </ul>
          - id: 6
            name: GitLab Ultimate for Open Source
            text: |
              <h3>GitLab for Open Source</h3>
              <ul>
                <li>GitLab for Open Source programs do not come with technical support. Technical support for open source can, however, be purchased at a significant discount by <a href="/sales/" data-ga-name="sales" data-ga-location="body">contacting Sales</a>.</li>
                <li>If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us" data-ga-name="gitlab support portal" data-ga-location="body">GitLab Support Portal</a> and select "<a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" data-ga-name="gitlab support portal" data-ga-location="body">Licensing and Renewals Problems</a>".</li>
              </ul>
          - id: 7
            name: GitLab Ultimate for Startups
            text: |
              <h3>GitLab for Startups</h3>
              <ul>
                <li>GitLab for Startups programs do not come with technical support. Technical support for startup can, however, be purchased at a significant discount by <a href="/sales/" data-ga-name="sales" data-ga-location="body">contacting Sales</a>.</li>
                <li>If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the <a href="https://support.gitlab.com/hc/en-us" data-ga-name="gitlab support portal" data-ga-location="body">GitLab Support Portal</a> and select "<a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" data-ga-name="gitlab support portal" data-ga-location="body">Licensing and Renewals Problems</a>".</li>
              </ul>
          - id: 8
            name: Legacy plan – Starter self-managed
            text: |
              <h3>GitLab Legacy Plan (Starter Self-managed)</h3>
              <ul>
                <li>GitLab Legacy plans come with <a href="/support/#standard-support-legacy">Standard Support</a> for Starter Self-managed and will reply within 24 hours on business days (24x5).</li>
                <li>Open a Support Ticket on the <a href="https://support.gitlab.com/">GitLab Support Portal</a>.</li>
              </ul>
          - id: 9
            name: Legacy plan – Bronze SaaS
            text: |
              <h3>GitLab Legacy Plan (Bronze SaaS)</h3>
              <ul>
                <li>GitLab Legacy plans come with <a href="/support/#standard-support-legacy">Standard Support</a> for Bronze SaaS and will reply within 24 hours on business days (24x5).</li>
                <li>Open a Support Ticket on the <a href="https://support.gitlab.com/">GitLab Support Portal</a>.</li>
              </ul>
          - id: 10
            name: US Federal – Self-managed
            text: |
              <h3>US Federal (SM Only)</h3>
              <ul>
                <li><a href="/support/us-federal-support/#us-federal-support">US Federal Support</a> is available by opening a Support Ticket on the <a href="https://gitlab-federal-support.zendesk.com/">GitLab Support Portal</a>.</li>
                <li>For emergency requests, see the note in the <a href="/support/us-federal-support/#us-federal-emergency-support">US Federal Support description</a>.</li>
              </ul>
          - id: 11
            name: Open Partners
            text: |
              <h3>Open Partners</h3>
              <ul>
                <li><a href="/support/#standard-support-legacy">Standard Support</a> or <a href="/support/#priority-support">Priority Support</a> is available for <a href="https://about.gitlab.com/handbook/support/partnerships/open.html">Open Partners</a> depending on the customer's license type.</li>
                <li>Open a Support Ticket using the <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000818199">GitLab Support Portal - Open Partner Form</a>.</li>
              </ul>
          - id: 12
            name: Select Partners
            text: |
              <h3>Select Partners</h3>
              <ul>
                <li><a href="/support/#priority-support">Priority Support</a> is available for <a href="https://about.gitlab.com/handbook/support/partnerships/select.html">Select Partners</a>.</li>
                <li>Open a Support Ticket using the <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000837100">GitLab Support Portal - Select Partner Form</a>.</li>
              </ul>
          - id: 13
            name: Alliance Partners
            text: |
              <h3>Alliance Partners</h3>
              <ul>
                <li><a href="/support/#priority-support">Priority Support</a> is available for <a href="https://about.gitlab.com/handbook/support/partnerships/alliance.html">Alliance Partners</a>.</li>
                <li>Open a Support Ticket using the <a href="https://support.gitlab.com/requests/new?ticket_form_id=360001172559">GitLab Support Portal - Alliance Partner Form</a>.</li>
              </ul>
  - name: support-copy-markdown
    data:
      block:
        - subtitle:
            id: managing-your-support-contacts
            text: Managing your support contacts
          text: |
            All individuals who will open support tickets must be associated with an organization that holds a valid GitLab subscription. If you are not pre-listed this will result in a rejection message detailing you how to manage your organizations support contacts. Please see our dedicated page on [Managing Support Contacts](/support/managing-support-contacts/) for how to let GitLab Support know who is authorized to contact support.
        - header: GitLab Support Service Levels
          id: gitlab-support-service-levels
          subtitle:
            id: trials-support
            text: Trials Support
          text: |
            Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider [contacting Sales](/sales/) to discuss options.
        - subtitle:
            id: standard-support-legacy
            text: Standard Support (Legacy)
          text: |
            Standard Support is included in Legacy GitLab self-managed Starter, and GitLab.com Bronze plans. It includes 'Next business day support' which means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours](/support/#hours-of-operation)).
        - subtitle:
            id: priority-support
            text: Priority Support
          text: |
            Priority Support is included with all self-managed and SaaS GitLab Premium and Ultimate purchases. These plans receive Tiered Support response times:

            | [Support Impact](/support/definitions/#definitions-of-support-impact)  | First Response Time SLA  | Hours  | How to Submit  |
            |---|---|---|---|
            | Emergency (Your GitLab instance is completely unusable)  | 30 minutes  | 24x7  | [Please trigger an emergency.](/support/#how-to-trigger-emergency-support)  |
            | Highly Degraded (Important features unavailable or extremely slow; No acceptable workaround)  | 4 hours  | 24x5 | Please submit requests through the [support web form.](https://support.gitlab.com/) |
            | Medium Impact  | 8 hours  | 24x5  | Please submit requests through the [support web form.](https://support.gitlab.com/)  |
            | Low Impact  | 24 hours  | 24x5  | Please submit requests through the [support web form.](https://support.gitlab.com/)  |

            Self-managed Premium and Ultimate may also receive:

            1. **Support for Scaled Architecture:** A Support Engineer will work with your technical team around any issues encountered after an implementation of a scaled architecture is completed in cooperation with our Customer Success team.
            2. **Upgrade assistance:** A Support Engineer will review and provide feedback on your upgrade plan to help ensure there aren't any surprises. Learn [how to schedule an upgrade](/support/scheduling-upgrade-assistance#how-do-i-schedule-upgrade-assistance).
        - subtitle:
            id: how-to-trigger-emergency-support
            text: How to trigger Emergency Support
          text: |
            To trigger emergency support you **must send a new email** to the emergency contact address provided with your GitLab license. When your license file was sent to your licensee email address, GitLab also sent a set of addresses to reach Support for emergency requests. You can also ask your Technical Account Manager or sales rep for these addresses.
              - **Note:** CCing the address on an existing ticket or forwarding it will not page the on-call engineer.

            It is preferable to [include any relevant screenshots/logs in the initial email](/support/general-policies/#working-effectively-in-support-tickets). However if you already have an open ticket that has since evolved into an emergency, please include the relevant ticket number in the initial email.

              - **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com/) before contacting Support.

            Once an emergency has been resolved, GitLab Support will close the emergency ticket. If a follow up is required post emergency, GitLab Support will either continue the conversation via a new regular ticket created on the customer's behalf, or via an existing related ticket. Support Emergencies are defined as "GitLab instance in production is unavailable or completely unusable". The following is non-exhaustive list of situations that are generally considered as emergencies based on that definition, and a list of high priority situations that generally do not qualify as an emergencies according to that definition.
            Emergency:

            - GitLab instance is "down", unavailable, or completely unresponsive (for GitLab.com outages, an [Incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management){target="_blank"} will be declared)
            - GitLab always shows 4xx or 5xx errors on every page
            - All users in an organization are unable to login to GitLab
            - All users in an organization are unable to access their work on GitLab

            Non-emergency:
            - A single user is unable to login to GitLab.
            - GitLab Runner or CI job execution is slower than usual.
            - A CI pipeline is failing on one or more projects but not _all_ projects.
            - One or more pages are not responding in browser, but most pages load successfully.
            - An access token has stopped working or SSH key has expired.
            - License/Subscription about to expire (there is a 14-day grace period following license expiration).
            - GitLab application is usuable but running slower than usual.
            - Security incident affecting a publicly-accessible and unpatched self-managed GitLab server.
            - A [Geo secondary](https://docs.gitlab.com/ee/administration/geo/#use-cases){target="_blank"} is unhealthy or down.
            - Elasticsearch integration is not working.

            To help Support accurately gauge the severity and urgency of the problem, please provide as many details about how the issue is impacting or disrupting normal business operation when submitting the emergency ticket.
        - header: Service Level Agreements
          id: service-level-agreements
          text: |
            - GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
            - The SLA times listed are the time frames in which you can expect the first response.
            - GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are not to be considered as an expected time-to-resolution.
        - header: Hours of Operation
          id: hours-of-operation
          subtitle:
            id: definitions-of-gitlab-global-support-hours
            text: Definitions of GitLab Global Support Hours
          text: |

            - 24x5 - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time.
            - 24x7 - For Emergency Support there is an engineer on-call 24 hours a day, 7 days a week.

            These hours are the SLA times when selecting 'All Regions' for 'Preferred Region for Support' within the GitLab Global Support Portal.
        - subtitle:
            id: effect-on-support-hours-if-a-preferred-region-for-support-is-chosen
            text: Effect on Support Hours if a preferred region for support is chosen
          text: |
            When submitting a new ticket, you may select a 'Preferred Region for Support'. This can help us assign Support Engineers from your region and can make the ticket more likely to receive replies in the specific region's business hours (rather than at night).
            For reference, the business hours for the various regions are as follows:

            - Asia Pacific (APAC): 09:00 to 21:00 AEST (Brisbane), Monday to Friday
            - Europe, Middle East, Africa (EMEA): 08:00 to 18:00 CET Amsterdam, Monday to Friday
            - Americas (AMER): 05:00 to 17:00 PT (US & Canada), Monday to Friday
        - subtitle:
            text: Phone and video call support
            id: phone-and-video-call-support
          text: |
            GitLab does not offer support via inbound or on-demand calls.

            GitLab Support Engineers communicate with you about your tickets primarily through updates in the tickets themselves. At times it may be useful and important to conduct a call, video call, or screensharing session with you to improve the progress of a ticket. The support engineer may suggest a call for that reason. You may also request a call if you feel one is needed. Either way, the decision to conduct a call always rests with the support engineer, who will determine:

              - whether a call is necessary; and
              - whether we have sufficient information for a successful call.

            Once the decision has been made to schedule a call, the support engineer will:
              - Send you a link (through the ticket) to our scheduling platform or, in the case of an emergency, a direct link to start the call.

              - Update you through the ticket with: (a) an agenda and purpose for the call, (b) a list of any actions that must be taken to prepare for the call, and (c) the maximum time allowed for the call. Please expect that the call will end as soon as the stated purpose has been achieved or the time limit has been reached, whichever occurs first.

            During a screensharing session Support Engineers will act as a trusted advisor: providing troubleshooting steps and inviting you to run commands to help gather data or help resolve technical issues. At no time will a GitLab Support Engineer ask to take control of your computer or to be granted direct access to your GitLab installation.
            **Note:** Calls scheduled by GitLab Support are on the Zoom platform. If you cannot use Zoom, you can request a Cisco Webex link. If neither of these work for you, GitLab Support can join a call on the following platforms: Microsoft Teams, Google Hangouts, Zoom, Cisco Webex. Other video platforms are not supported.
            **Please Note:** Attempts to reuse a previously-provided scheduling link to arrange an on-demand call will be considered an abuse of support, and will result in such calls being cancelled.
        - subtitle:
            text: Resources
            id: resources
          text: |
            Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our get help page.
            Or check our some of our other support pages:
            - [Customer Satisfaction](/support/customer-satisfaction/)
            - [General Support Rules](/support/general-policies/)
            - [GitLab.com Specific Support Policies](/support/gitlab-com-policies/)
            - [Support Definitions](/support/definitions/)
            - [Support Portal](/support/portal/)
            - [US Federal Support](/support/us-federal-support/)
  - name: support-note
    data:
      id: more-resources
      icon:
        name: bulb
        variant: marketing
        alt: Bulb Icon
      title: More Help
      text: |
        For additional resources for getting help, reporting issues, requesting features, and so forth are listed on our get help page.
      cta:
        text: Visit Get Help page
        href: /get-help/
        data_ga_name: visit get help page
        data_ga_location: body
