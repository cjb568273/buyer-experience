---
  title: GitLab for Open Source
  description: Open source communities benefit from The DevSecOps Platform.
  image_title: /nuxt-images/open-graph/open-source-card.png
  image_alt: GitLab for Open Source
  twitter_image: /nuxt-images/open-graph/open-source-card.png
  template: 'industry'
  breadcrumbs:
    - title: GitLab Open Source
      href: /solutions/open-source/
      data_ga_name: open source
      data_ga_location: breadcrumb
    - title: Open Source Partners
      href: /solutions/open-source/partners
      data_ga_name: open source partners
      data_ga_location: breadcrumb

  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Open Source Partners
        subtitle: Partner with GitLab and strengthen the open source ecosystem.
        primary_btn:
          url: /handbook/marketing/community-relations/community-programs/opensource-program/#gitlab-open-source-partners
          text: Learn more
          data_ga_name: join open source program
          data_ga_location: header
        image:
          image_url: /nuxt-images/open-source/open-source-partners-hero.png
          alt: "Image: gitlab for open source"
          bordered: true
    - name: 'overview-blocks'
      data: 
        primary:
          header: Benefits
          list:
            - Public recognition as a GitLab Open Source Partner
            - Direct line of communication to GitLab
            - Assistance migrating additional infrastructure to GitLab
            - Exclusive invitations to participate in GitLab events
            - Opportunities to meet with and learn from other open source partners
            - Visibility and promotion through GitLab marketing channels
        secondary:
          header: Requirements
          list: 
            - Engage in co-marketing efforts with GitLab
            - Complete a public case study about their innovative use of GitLab
            - Plan and participate in joint initiatives and events
        cta:
          header: Resources
          description: GitLab Open Source Partners join a community of peers—sharing knowledge, addressing challenges, and building the future of open source on GitLab.
          link: 
            text: Learn more
            href: https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-open-source-partners
    - name: 'partners-grid-full'
      data:
        header: Our open source partners
        subheader: These communities are building the future of open source on GitLab.
        partners:
          - logo: /nuxt-images/open-source/ska.png
            alt: ska Logo
            href: https://www.skatelescope.org/
          - logo: /nuxt-images/open-source/gnome.png
            alt: gnome Logo
            href: https://www.gnome.org/
          - logo: /nuxt-images/open-source/debian.png
            alt: debian Logo
            href: https://www.debian.org/
          - logo: /nuxt-images/open-source/KDE.png
            alt: kde Logo
            href: https://www.kde.org/
          - logo: /nuxt-images/open-source/drupal.png
            alt: drupal Logo
            href: https://www.drupal.org/
          - logo: /nuxt-images/open-source/xorg.png
            alt: X org Logo
            href: https://www.x.org/
          - logo: /nuxt-images/open-source/xfce.png
            alt: xfce Logo
            href: https://www.xfce.org/
          - logo: /nuxt-images/open-source/finos.png
            alt: finos Logo
            href: https://www.finos.org/
          - logo: /nuxt-images/open-source/opencores.png
            alt: opencores Logo
            href: https://opencores.org/
          - logo: /nuxt-images/open-source/vlc.png
            alt: vlc Logo
            href: http://www.videolan.org/

          - logo: /nuxt-images/open-source/arm.png
            alt: arm Logo
            href: https://developer.arm.com/tools-and-software/open-source-software
          - logo: /nuxt-images/open-source/kali.png
            alt: kali Logo
            href: https://www.kali.org/
          - logo: /nuxt-images/open-source/haskell.png
            alt: haskell Logo
            href: https://www.haskell.org/
          - logo: /nuxt-images/open-source/soleil.png
            alt: soleil Logo
            href: https://www.synchrotron-soleil.fr/en
          - logo: /nuxt-images/open-source/archlinux.png
            alt: archlinux Logo
            href: https://archlinux.org/
          - logo: /nuxt-images/open-source/fedora.png
            alt: fedora Logo
            href: https://getfedora.org/
          - logo: /nuxt-images/open-source/centos.png
            alt: centos Logo
            href: https://www.centos.org/
          - logo: /nuxt-images/open-source/free-desktop.png
            alt: free desktop Logo
            href: https://www.freedesktop.org/

          - logo: /nuxt-images/open-source/inkscape.png
            alt: inkscape Logo
            href: https://inkscape.org/
          - logo: /nuxt-images/open-source/f-droid.png
            alt: f-droid Logo
            href: https://www.f-droid.org/


            
    - name: open-source-blog
      data:
        no_background: true
        carousel: false
        header: Creating open source software. Faster.
        subheader: See what open source partners are achieving with GitLab
        link:
          text: More Open Source posts
          url: /blog/categories/open-source/
        entries:
          - description: Come on in! Drupal is moving to GitLab
            link: 
              href: /blog/2018/08/16/drupal-moves-to-gitlab/
            image: /nuxt-images/open-source/Thumb-Drupal.jpg
          - description: 'Why the KDE community is #movingtogitlab'
            link:
              href: /blog/2020/06/29/welcome-kde/
            image: /nuxt-images/open-source/Thumb-KDE.jpg
          - description: 'GNOME: two years after the move to GitLab'
            link:
              href: /blog/2020/09/08/gnome-follow-up/
            image: /nuxt-images/open-source/Thumb-Gnome.jpg
          - description:  OpenCores come to GitLab 
            link:
              href: /blog/2019/12/03/welcoming-opencores-to-gitlab/
            image: /nuxt-images/open-source/thumb-OpenSource-OpenCore.jpg
          - description: How GitLab helped Kali Linux attract a growing number of community contributions 
            link:
              href: /blog/2021/02/18/kali-linux-movingtogitlab/
            image: /nuxt-images/open-source/Thumb-KaliLinux.jpg
          - description: KDE empowers open source community with GitLab
            link:
              href: https://www.youtube.com/watch?v=aLDFlXSI0Qs&feature=youtu.be
              text: Watch webcast
            image: /nuxt-images/open-source/infinity.jpg
            type: Webcast
            icon: webcast
    - name: solutions-cards
      data:
        title: Explore other ways GitLab can help open source developers
        column_size: 4
        link :
          url: /solutions/
          text: Explore Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: Source Code Management
            description: GitLab makes Source Code Management easy
            icon:
              name: cog-code
              alt: cog-code Icon
              variant: marketing
            href: /stages-devops-lifecycle/source-code-management/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Continuous Integration and Delivery
            description: Make software delivery repeatable and on-demand
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /features/continuous-integration/
            data_ga_name: Continuous Integration and Delivery learn more
            data_ga_location: body
          - title: Continuous Software Security
            description: Shift security left with built-in DevSecOps
            icon:
              name: devsecops
              alt: devsecops Icon
              variant: marketing
            href: /solutions/continuous-software-security-assurance/
            data_ga_name: Continuous Software Security learn more
            data_ga_location: body

