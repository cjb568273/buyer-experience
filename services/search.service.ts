import { SEARCH_TYPE } from '../common/constants';

const basePath = 'https://search-api.swiftype.com/api/v1/public/engines';

/**
 * Public keys for Swiftype site-search API
 */
const ENGINE_KEYS = Object.freeze([
  {
    key: '6meAsJr1HTFB8FoyaYAv',
    value: SEARCH_TYPE.MARKETING,
  },
  {
    key: '5NUxKQM5PaEFZBBLVWhm',
    value: SEARCH_TYPE.HANDBOOK,
  },
  {
    key: 'rC7jfz7JeLu91vDG-rUF',
    value: SEARCH_TYPE.BLOG,
  },
]);

/**
 * Body for Swiftype's search -> Documentation for site-search https://swiftype.com/documentation/site-search/searching
 */
export class SearchBody {
  /**
   * Public key that defines which engine will be searched
   * @private
   */
  // eslint-disable-next-line babel/camelcase
  engine_key?: string;

  /**
   * Search term
   */
  q: string;

  /**
   * Pagination's current page
   */
  page?: number;

  /**
   * Number of items per page
   */
  // eslint-disable-next-line babel/camelcase
  per_page?: number;

  /**
   * Attribute that defines if the search term should return a spelling suggestion -> https://swiftype.com/documentation/site-search/searching/spelling
   */
  spelling?: string;

  /**
   * Field to be sorted by
   */
  // eslint-disable-next-line babel/camelcase
  sort_field?: { page: string };

  /**
   * Sorting direction, it can be 'desc' or 'asc'
   */
  // eslint-disable-next-line babel/camelcase
  sort_direction?: { page: 'desc' | 'asc' };

  constructor(body: SearchBody) {
    this.q = body.q;
    this.page = body.page;
    this.per_page = body.per_page;
    this.spelling = body.spelling;
    this.sort_direction = body.sort_direction;
    this.sort_field = body.sort_field;
  }
}

/**
 * HTTP service to execute the search
 * @param searchBody
 */
export const search = (searchBody: SearchBody, searchType: string) => {
  return new Promise((resolve, reject) => {
    const keyObj = ENGINE_KEYS.find((obj) => obj.value === searchType);

    if (!keyObj) {
      return reject(new Error('Invalid search engine'));
    }

    const body: SearchBody = { ...searchBody, engine_key: keyObj?.key };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    };

    return fetch(`${basePath}/search`, requestOptions).then((res) =>
      resolve(res.json()),
    );
  });
};
